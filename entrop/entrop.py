from collections import Counter
import time
from multiprocessing import Pool
from math import log

star_time = time.time()
def uniq_count(message):
    return list(Counter(message).values())

def ent(message):
    return -sum([e/len(message)*log(2,(e/len(message))) for e in uniq_count(message)])


def counter(file):
    qwe = 0
    while file:
        data = file.read(1024*1024)
        qwe = qwe + ent(data)
        print(qwe)

if __name__ == '__main__':
    file = open('/home/nikita/Рабочий стол/sem2/sample.txt', 'rb')
    pool = Pool(processes=2)
    pool.map_async(counter(file),file)
    pool.close()
    pool.join()
    file.close()
    print(time.time()-star_time)
