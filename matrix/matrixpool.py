import random
from multiprocessing import Pool
from timeit import default_timer
start_time = default_timer()

def paral(matrix):
  matrix1 = matrix
  s = 0
  t = []
  c = []
  for z in range(0,len(matrix)):
    for j in range(0,len(matrix)):
      for i in range(0,len(matrix)):
        s = s + matrix[z][i] * matrix[i][j]
      t.append(s)
      s = 0
    c.append(t)
    t = []
  return c

def prepare(li):
  C = []
  matrix = li
  C.append(paral(matrix))
  return C

r = int(2)
matrix1 = [[random.randint(0, 10) for i in range(r)] for j in range(r)]
matrix2 = [[random.randint(0, 10) for k in range(r)] for l in range(r)]
li=(matrix1,matrix2)


if __name__ == '__main__':
    pool = Pool()
    print(pool.map(prepare,li))
    pool.close()
    pool.join()
    stop_time = default_timer()
    print(stop_time-start_time)

